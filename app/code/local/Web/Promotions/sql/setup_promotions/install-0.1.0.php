<?php

$installer = $this;
$table = $installer->getTable('webpromotions/promotions');

$installer->startSetup();

$installer->getConnection()->dropTable($table);

$tablePromotions = $installer->getConnection()
    ->newTable($table)
    ->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity'  => true,
        'nullable'  => false,
        'primary'   => true,
        ))
    ->addColumn('promotions_name', Varien_Db_Ddl_Table::TYPE_VARCHAR, '255', array(
        'nullable'  => false,
        ))
    ->addColumn('description', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
        'nullable'  => false,
        ))
    ->addColumn('image', Varien_Db_Ddl_Table::TYPE_VARCHAR, '255', array(
        'nullable'  => false,
        ))
    ->addColumn('is_enabled', Varien_Db_Ddl_Table::TYPE_TINYINT, '255', array(
        'nullable'  => false,
        ))
    ->addColumn('sort_order', Varien_Db_Ddl_Table::TYPE_VARCHAR, '255', array(
        'nullable'  => false,
        ))
    ->addColumn('static_blocks', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
        'nullable'  => false,
        ))
    ->addColumn('category', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
        'nullable'  => false,
//        ))
//    ->addColumn('products', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
//        'nullable'  => false,
    ));

$installer->getConnection()->createTable($tablePromotions);
//
//$tableStaticBlocks = $installer->getConnection()
//    ->newTable('webpromotions/staticblocks')
//    ->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
//        'identity'  => true,
//        'nullable'  => false,
//        'primary'   => true,
//    ))
//    ->addColumn('name_block', Varien_Db_Ddl_Table::TYPE_VARCHAR, '255', array(
//        'nullable'  => false,
//    ))
//;

$installer->endSetup();