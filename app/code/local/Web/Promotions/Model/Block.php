<?php

class Web_Promotions_Model_Block
{
    protected $_options = null;
    protected $_type;

    public function getOptions()
    {
        if ($this->_options === null) {
            $this->_options = array();
            
            $this->_options = $this->getData($this->getDataArray());
            
        }
        return $this->_options;
    }

    public function toOptionArray($type)
    {
        $this->_type = $type;
        $options = array();

        foreach ($this->getOptions() as $value => $label) {
            $options[] = array(
                'label'  => $label,
                'value'  => (int) $value,
            );
        }

        return $options;
    }

    public function getDataArray($type = false)
    {
        $dataArray = array();

        if($type != false){
            $this->_type = $type;
        }

        switch($this->_type)
        {
            case 'catalog/category':
                $catalog = Mage::getSingleton($this->_type)->getCollection()->getData();

                $dataArray[] = array(
                    'name' => 'None',
                    'id'   => '0'
                );

                foreach($catalog  as $item){
                    $dataArray[] = array(
                        'name' => Mage::getModel($this->_type)->setStoreId(Mage::app()->getStore()->getId())
                            ->load($item['entity_id'])
                            ->getName(),
                        'id'   => $item['entity_id']
                    );
                }
                break;

            case 'cms/block':
                $blocks = Mage::getSingleton($this->_type)->getCollection()->getData();

                $dataArray[] = array(
                    'name' => 'None',
                    'id'   => '0'
                );

                foreach($blocks as $item){

                    $dataArray[] = array(
                        'name' => $item['title'],
                        'id'   => $item['block_id']
                    );
                }
             break;

            //            case 'catalog/product':
//                $catalog = Mage::getSingleton($this->_type)->getCollection()->getData();
//
//                foreach($catalog  as $item){
//                    $dataArray[] = array(
//                        'name' => Mage::getModel($this->_type)->setStoreId(Mage::app()->getStore()->getId())
//                            ->load($item['entity_id'])
//                            ->getName(),
//                        'id'   => $item['entity_id']
//                    );
//                }
//                break;

        }
        return $dataArray;
    }

    protected function getData($dataArray)
    {
        foreach ($dataArray as $layout) {
            $this->_options[$layout['id']] = $layout['name'];
        }

        return $this->_options;
    }

}