<?php

class Web_Promotions_Block_Carousel_Front extends Mage_Core_Block_Template
{

    public function _prepareLayout()
    {
        return parent::_prepareLayout();
    }

    public function getPromotionsCollection()
    {
        $id = $this->getRequest()->getParam('id');

        return Mage::helper('webpromotions/front')->getPromotionsData($id);
    }

    public function getBlocks($blocks_id)
    {
        try{
            $blocks = array();
            foreach($blocks_id as $id){
                if($id != '0'){
                    $blocks[] = Mage::getSingleton('cms/block')->load($id)->getData();
                }
            }
            return $blocks;
        }catch(Exception $e)
        {
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
        }
    }

    protected function getCategory($category_id)
    {
        foreach ($category_id as $id) {
            $category[] = Mage::getSingleton('catalog/category')->load($id)->getData();
        }
    }

//    public function getProducts($category_id)
//    {
//
//    }

    public function getPromotions()
    {
        try{
            $categoryId = $this->getCategoryId();

            $categoryData = Mage::getSingleton('webpromotions/promotions')->getCollection()->getData();

            $data = array();
            foreach($categoryData as $value)
            {
                $category = unserialize($value['category']);

                if(in_array($categoryId, $category))
                {
                    $data[] = $value;
                }
            }

            return $data;
        }catch(Exception $e){
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
        }
    }

    protected function getCategoryId()
    {
        return Mage::registry('current_category') ? Mage::registry('current_category')->getId() : null;
    }
}
